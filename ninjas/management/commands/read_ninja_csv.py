from django.core.management import base
from django.contrib import auth

from ninjas.models import Ninja, Parent
from dojo import models as dojo_models

import csv


class Command(base.BaseCommand):
    """Import a Google Forms-generated Ninja sign-up CSV.

    The CSV's format is hard-coded. I don't really have plans to fix this
    limitation because I never actually know what the next term's form will
    look like.

    This command takes a list of paths to CSV files, and imports all rows.
    """
    def handle(self, *args, **options):
        try:
            csv_file = open(args[0], 'rb')
            csv_reader = csv.DictReader(csv_file)
        except:
            raise base.CommandError("Issues with File I/O: %s" % csv_file)
        for row in csv_reader:
            self.process_row(row, args[1])

    def process_row(self, row, term):
        """
        Process a single CSV row, generating the necessary database
        objects.
        """
        # First, try to match the Parent with an existing user.
        # This should be easy to do with the email address.
        try:
            p_user = auth.get_user_model().objects.get(
                email=row['Parent/guardian email'].strip()
            )
        except:
            # No user found. Create a new one from the information we have.
            p_user = auth.get_user_model()()
            p_user.email = row['Parent/guardian email'].strip()
            p_user.username = ("{u.first_name}.{u.last_name}".format(u=p_user)
                               .lower())

            # The account will be pseudo-locked by default.
            p_user.set_unusable_password()

        p_user.first_name = row['Parent/guardian first name'].strip()
        p_user.last_name = row['Parent/guardian family name'].strip()
        p_user.full_clean()
        p_user.save()
        p_user.groups.add(auth.models.Group.objects.get(name='Parents'))

        # Check to see if the user has an associated Parent object.
        try:
            parent = Parent.objects.get(user=p_user)
        except:
            parent = Parent()
            parent.user = p_user

        parent.mobile = row['Phone number']
        parent.save()

        # Look for a Ninja. This is little bit harder.
        #
        # IF the Ninja has provided an email address, and it's different
        # to their parents email address, we'll use that.
        #
        # IF they haven't, we'll use:
        #   parent_email_account+ninja_first_name@parent_email_domain
        #
        # Many mainstream mail providers (Gmail, Outlook.com) will send
        # mail to the base account if mail like this is received.
        ninja_email = row["Ninja's email address"]

        if ninja_email == '' or ninja_email == p_user.email:
            email_parts = p_user.email.split('@')
            ninja_email = "{account}+{name}@{domain}".format(
                account=email_parts[0],
                name=row["Ninja's first name"].lower(),
                domain=email_parts[1]
            )

        # Now that we've got an email to check against, see if there's a user
        # belonging to the ninja.
        try:
            n_user = auth.get_user_model().objects.get(email=ninja_email)
        except:
            # No user found. Create a new one from the information we have.
            n_user = auth.get_user_model()()
            n_user.first_name = row["Ninja's first name"].strip()
            n_user.last_name = row["Ninja's family name"].strip()
            n_user.email = ninja_email
            n_user.username = ("{u.first_name}.{u.last_name}".format(u=n_user)
                               .lower())

            # The account will be pseudo-locked by default.
            n_user.set_unusable_password()
            n_user.full_clean()
            n_user.save()
            n_user.groups.add(auth.models.Group.objects.get(name='Ninjas'))

        try:
            ninja = Ninja.objects.get(user=n_user)
        except:
            ninja = Ninja(
                user=n_user
            )

        # Fill in missing info.
        ninja.gender = row['Gender'][0]
        ninja.school_year = row['School year']
        ninja.laptop = row['Laptop'] == 'Yes'  # fake boolean
        if row['Allergies/dietary restrictions'] != '':
            ninja.allergies = row['Allergies/dietary restrictions']
        ninja.attended_old_dojo = row['Been before?'] == 'Yes'
        ninja.full_clean()
        ninja.save()

        # Add parents and term enrolments
        ninja.parents.add(parent)
        ninja.enrolments.add(dojo_models.Term.objects.get(pk=term))
        ninja.full_clean()
        ninja.save()
