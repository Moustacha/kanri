from django.db import models
from django.conf import settings


class Parent(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        help_text='The user associated with this parent.',
    )

    # FIXME: (length) work out why clean() runs after length
    # checks are executed.
    mobile = models.CharField(
        max_length=13,
        help_text="The parent's mobile contact number.",
    )

    def __str__(self):
        return self.user.get_full_name()
