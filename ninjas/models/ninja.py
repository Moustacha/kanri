from django.db import models
from django.conf import settings


class Ninja(models.Model):
    MALE = 'M'
    FEMALE = 'F'

    GENDER_CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        help_text='The user associated with this ninja.',
    )

    gender = models.CharField(
        max_length=1,
        choices=GENDER_CHOICES
    )

    school_year = models.PositiveSmallIntegerField(
        help_text='The ninja\'s school year.'
    )

    laptop = models.BooleanField(
        default=False,
        help_text='Does this ninja have a laptop they can bring with them?'
    )

    allergies = models.CharField(
        max_length=50,
        blank=True,
        help_text='A list of allergies this ninja has.'
    )

    attended_old_dojo = models.BooleanField(
        default=False,
        help_text='The Ninja has attended Dojos before persistent records.'
    )

    parents = models.ManyToManyField(
        'ninjas.Parent',
        help_text='The Ninja\'s listed parent.'
    )

    applied_dojos = models.ManyToManyField(
        'dojo.Term',
        related_name='applied_ninjas',
        help_text="Dojo terms the Ninja has applied to be in."
    )

    enrolled_dojos = models.ManyToManyField(
        'dojo.Term',
        related_name='enrolled_ninjas',
        help_text="Dojo terms the Ninja has enroled in."
    )

    @property
    def age(self):
        return self.school_year + 5

    def supervision_required(self):
        return self.school_year < 6

    def returning(self):
        # in the future, this'll handle old terms too.
        return self.attended_old_dojo

    def not_attended_session(self):
        return (not self.returning()) and (not self.attendance_set.all())

    def __str__(self):
        return self.user.get_full_name()

    returning.boolean = True  # django admin setting
    not_attended_session.boolean = True  # ditto
    supervision_required.boolean = True  # ditto

    class Meta:
        ordering = ['user__first_name']
