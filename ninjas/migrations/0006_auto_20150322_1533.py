# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0007_role_advertised'),
        ('ninjas', '0005_ninja_enrolments'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ninja',
            name='enrolments',
        ),
        migrations.AddField(
            model_name='ninja',
            name='applied_terms',
            field=models.ManyToManyField(help_text=b'Dojo terms the Ninja has applied to be in.', related_name='applied_ninjas', to='dojo.Term'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='ninja',
            name='enrolled_terms',
            field=models.ManyToManyField(help_text=b'Dojo terms the Ninja has enroled in.', related_name='enrolled_ninjas', to='dojo.Term'),
            preserve_default=True,
        ),
    ]
