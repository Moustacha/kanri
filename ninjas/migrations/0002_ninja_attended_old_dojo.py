# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ninjas', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='ninja',
            name='attended_old_dojo',
            field=models.BooleanField(default=False, help_text=b'The Ninja has attended Dojos before persistent records.'),
            preserve_default=True,
        ),
    ]
