# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Ninja',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gender', models.CharField(max_length=1, choices=[(b'M', b'Male'), (b'F', b'Female')])),
                ('age', models.PositiveSmallIntegerField(help_text=b"The ninja's age.")),
                ('laptop', models.BooleanField(default=False, help_text=b'Does this ninja have a laptop they can bring with them?')),
                ('allergies', models.CharField(help_text=b'A list of allergies this ninja has.', max_length=50, blank=True)),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, help_text=b'The user associated with this ninja.')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
