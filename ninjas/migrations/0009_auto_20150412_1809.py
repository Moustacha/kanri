# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ninjas', '0008_auto_20150322_1557'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ninja',
            options={'ordering': ['user__first_name']},
        ),
    ]
