from django.contrib import admin
import models


@admin.register(models.Ninja)
class NinjaAdmin(admin.ModelAdmin):
    list_display = ['user', 'gender', 'age', 'returning',
                    'laptop', 'allergies']
    list_filter = ['gender', 'laptop']


@admin.register(models.Parent)
class ParentAdmin(admin.ModelAdmin):
    list_display = ['user', 'mobile']
