function attendance_toggle(in_btn, out_btn) {
    $('.btn-' + in_btn + '[data-ninja-id="' + ninja_id + '"]').removeClass('hidden')
    $('.btn-' + out_btn + '[data-ninja-id="' + ninja_id + '"]').addClass('hidden')
}


function mark_attendance(ev, url, session_id, here) {
    ninja_id = $(ev.target).data('ninja-id');
    orig_text = $(ev.target).text()
    $(ev.target).addClass('disabled').text('Working...');

    $.post(url, {
        'session': session_id,
        'ninja': ninja_id,
        'here': here
    }, function(data, status, jqxhr){
        if (data.success === true) {
            if (here) {
                attendance_toggle('undo', 'attending')
            }else{
                attendance_toggle('attending', 'undo')
            }
        } else if (data.error === true) {
            $(ev.target).text('Error')
            $(ev.target).removeClass('btn-success')
            $(ev.target).addClass('btn-danger')
        }
    }, 'json');

    $(ev.target).removeClass('disabled').text(orig_text);
}