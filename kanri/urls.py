from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^roster/', include('roster.urls', namespace='roster')),
    url(r'^mentors/', include('mentors.urls', namespace='mentors')),
    url(r'^dojo/', include('dojo.urls', namespace='dojo')),
    url(r'^attendance/', include('attendance.urls', namespace='attendance')),
    url(r'^badges/', include('badges.urls', namespace='badges')),

    # Auth
    url(r'^login/', 'django.contrib.auth.views.login', name='login'),

    url(
        r'^logout/',
        'django.contrib.auth.views.logout_then_login',
        name='logout'
    ),

    # Dashboard app collects all other URLs
    url(r'', include('dashboard.urls', namespace='dashboard')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
