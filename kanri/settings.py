"""
Django settings for kanri project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

import os
from django.contrib.messages import constants as messages
from django.core import urlresolvers

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Secret key
SECRET_KEY = os.environ.get('KANRI_SECRET_KEY')

# Debugging
DEBUG = os.environ.get('KANRI_DEBUG', False) == 'true'
TEMPLATE_DEBUG = DEBUG

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'bootstrap3',
    'crispy_forms',
    'fontawesome',

    'dashboard',

    'mentors',
    'dojo',
    'roster',
    'ninjas',
    'attendance',
    'badges',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.messages.context_processors.messages',
    'django.contrib.auth.context_processors.auth',
)

ROOT_URLCONF = 'kanri.urls'
WSGI_APPLICATION = 'kanri.wsgi.application'


# Database stuff
DB_ENVIRONMENT = os.environ.get('KANRI_DB_ENV')
DB_ENVIRONMENTS = {
    'dev': {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    },
    'deploy': {
        'default': {
            'ENGINE': os.environ.get('KANRI_DB_BACKEND'),
            'NAME': os.environ.get('KANRI_DB_NAME'),
            'USER': os.environ.get('KANRI_DB_USER'),
            'PASSWORD': os.environ.get('KANRI_DB_PASSWORD'),
            'HOST': os.environ.get('KANRI_DB_HOST'),
            'PORT': os.environ.get('KANRI_DB_PORT'),
        }
    }
}

DATABASES = DB_ENVIRONMENTS[DB_ENVIRONMENT]

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-au'
TIME_ZONE = 'Australia/Perth'
USE_I18N = True
USE_L10N = True
USE_TZ = True

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates').replace('\\', '/'),
)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static asset configuration
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

MEDIA_ROOT = 'mediafiles'
MEDIA_URL = '/media/'

# Proxy stuff
USE_X_FORWARDED_HOST = True

# Customised django.contrib.messages for bootstrap
MESSAGE_TAGS = {
    messages.ERROR: 'danger',
}

# Crispy Forms
CRISPY_TEMPLATE_PACK = 'bootstrap3'

# Auth
LOGIN_URL = urlresolvers.reverse_lazy('login')
