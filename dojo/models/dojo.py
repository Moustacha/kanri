from django.db import models


class Dojo(models.Model):
    name = models.CharField(
        max_length=50,
        help_text='The Dojo\'s unique, human-readable name.',
        unique=True
    )

    location = models.TextField(
        help_text='The dojo\'s physical location.'
    )

    def __str__(self):
        return self.name
