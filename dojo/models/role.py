from django.db import models


class Role(models.Model):
    name = models.CharField(
        max_length=40,
        help_text='The role\'s name.',
    )

    description = models.TextField(
        help_text='A brief description of the role.',
        blank=True,
    )

    advertised = models.BooleanField(
        default=False,
        help_text='Should this role be advertised to mentors?'
    )

    dojo = models.ForeignKey(
        'dojo.Dojo',
        help_text='The Dojo that the role is relevant to.',
    )

    class Meta:
        unique_together = ['name', 'dojo']

    def __str__(self):
        return "{r.name} at {r.dojo}".format(r=self)
