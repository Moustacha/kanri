from django.db import models


class Session(models.Model):
    REGULAR = 'REG'
    TRAINING = 'TRN'
    MEETING = 'MET'
    SOCIAL = 'SOC'

    SESSION_TYPE_CHOICES = (
        (REGULAR, 'Regular Dojo Session'),
        (TRAINING, 'Mentor Training Session'),
        (MEETING, 'Planning/Meeting Session'),
        (SOCIAL, 'Social Event'),
    )

    name = models.CharField(
        max_length=50,
        blank=True,
        help_text='The name of the session.',
    )

    description = models.TextField(
        help_text='A short description of the session.',
        blank=True
    )

    start = models.DateTimeField(
        help_text='The starting date/time of the session.'
    )

    finish = models.DateTimeField(
        help_text='The finishing date/time of the session.'
    )

    session_type = models.CharField(
        max_length=3,
        choices=SESSION_TYPE_CHOICES,
        help_text='The type of session being run.'
    )

    term = models.ForeignKey(
        'dojo.Term',
        help_text='The term during which this session will run.'
    )

    def __str__(self):
        if self.name:
            n = self.name
        else:
            n = self.get_session_type_display()
        return "{n} at {s.term.dojo}".format(n=n, s=self)

    class Meta:
        ordering = ['start']
