# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0005_auto_20150314_0848'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='session',
            options={'ordering': ['start']},
        ),
        migrations.RemoveField(
            model_name='dojo',
            name='mentors',
        ),
    ]
