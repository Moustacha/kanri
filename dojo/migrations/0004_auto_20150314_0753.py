# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0009_remove_mentor_enrolments'),
        ('dojo', '0003_auto_20150312_1343'),
    ]

    operations = [
        migrations.AddField(
            model_name='dojo',
            name='mentors',
            field=models.ManyToManyField(help_text=b'The mentors that have registered interest in this dojo.', to='mentors.Mentor', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='session',
            name='session_type',
            field=models.CharField(help_text=b'The type of session being run.', max_length=3, choices=[(b'REG', b'Regular Dojo Session'), (b'TRN', b'Mentor Training Session'), (b'MET', b'Planning/Meeting Session'), (b'SOC', b'Social Event')]),
            preserve_default=True,
        ),
    ]
