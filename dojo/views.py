from django.views import generic

import models
import forms

from mentors import models as mentors_models


class RoleView(generic.TemplateView):
    template_name = 'dojo/roles.html'

    def get_context_data(self, *args, **kwargs):
        c = super(RoleView, self).get_context_data(*args, **kwargs)
        mentor = mentors_models.Mentor.objects.get(user=self.request.user)
        others = models.Role.objects.exclude(
                id__in=[r.id for r in mentor.approved_roles.all()],
            ).exclude(
                id__in=[r.id for r in mentor.applied_roles.all()]
            )
        c['approved'] = mentor.approved_roles.all()
        c['applied'] = mentor.applied_roles.all()
        c['other'] = others.filter(advertised=True)
        c['unadvertised'] = others.filter(advertised=False)
        c['select_form'] = forms.RoleSelectForm()

        return c
