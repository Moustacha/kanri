from django.conf.urls import patterns, url
from django.contrib import admin

import views

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', views.BadgeListView.as_view(), name='list'),
    url(r'^leaderboard/$', views.BadgeLeaderboard.as_view(), name='leaderboard'),
    url(r'^timeline/$', views.AwardTimeline.as_view(), name='timeline'),
)
