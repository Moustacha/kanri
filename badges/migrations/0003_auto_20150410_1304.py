# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0002_badgeaward_notes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='badge',
            name='short_description',
            field=models.CharField(help_text=b'A short description of what this badge means (e.g. completed all Scratch tutorials).', max_length=50),
            preserve_default=True,
        ),
    ]
