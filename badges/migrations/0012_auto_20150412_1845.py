# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0011_auto_20150412_1827'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='attachment',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['name']},
        ),
        migrations.AlterField(
            model_name='attachment',
            name='name',
            field=models.CharField(help_text=b"The attachment's human-readable name.", unique=True, max_length=50),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='badge',
            name='name',
            field=models.CharField(help_text=b"The badge's visible name", unique=True, max_length=30),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(help_text=b'The name of the category.', unique=True, max_length=50),
            preserve_default=True,
        ),
    ]
