# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0007_auto_20150410_1634'),
    ]

    operations = [
        migrations.AlterField(
            model_name='badge',
            name='name',
            field=models.CharField(help_text=b"The badge's visible name", max_length=30),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='badge',
            name='slug',
            field=models.SlugField(help_text=b'A unique slug for the badge.', unique=True, max_length=30),
            preserve_default=True,
        ),
    ]
