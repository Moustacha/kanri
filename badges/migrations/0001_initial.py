# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ninjas', '0008_auto_20150322_1557'),
        ('mentors', '0013_auto_20150406_1939'),
    ]

    operations = [
        migrations.CreateModel(
            name='Badge',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b"The badge's visible name", max_length=20)),
                ('image', models.ImageField(help_text=b'The badge icon. Square transparent PNGs please.', upload_to=b'', blank=True)),
                ('short_description', models.TextField(help_text=b'A short description of what this badge means (e.g. completed all Scratch tutorials).', max_length=50)),
                ('requirements', models.TextField(help_text=b'In-depth requirements. What do you need to do to get this badge?', max_length=4096)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BadgeAward',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(help_text=b'The time at which the badge was awarded.', auto_now_add=True)),
                ('awarder', models.ForeignKey(help_text=b'The Mentor that awarded the badge to this Ninja.', to='mentors.Mentor')),
                ('badge', models.ForeignKey(help_text=b'The badge to award.', to='badges.Badge')),
                ('ninja', models.ForeignKey(help_text=b'The Ninja to award the badge to.', to='ninjas.Ninja')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='badge',
            name='recipients',
            field=models.ManyToManyField(help_text=b'The Ninjas that have received the badge.', to='ninjas.Ninja', through='badges.BadgeAward'),
            preserve_default=True,
        ),
    ]
