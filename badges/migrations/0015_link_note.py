# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badges', '0014_category_links'),
    ]

    operations = [
        migrations.AddField(
            model_name='link',
            name='note',
            field=models.CharField(help_text=b'An optional note about the link.', max_length=100, blank=True),
            preserve_default=True,
        ),
    ]
