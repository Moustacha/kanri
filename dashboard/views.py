from django.views import generic

import mentors
import ninjas


class DashboardView(generic.TemplateView):
    template_name = 'dashboard/dashboard.html'

    def get_context_data(self, *args, **kwargs):
        c = super(DashboardView, self).get_context_data(*args, **kwargs)
        try:
            c['mentor'] = mentors.models.Mentor.objects.get(
                user=self.request.user)
            c['parent'] = ninjas.models.Parent.objects.get(
                user=self.request.user)
            c['ninja'] = ninjas.models.Ninja.objects.get(
                user=self.request.user)
        except:
            pass
        return c
