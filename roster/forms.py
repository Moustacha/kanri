from django import forms

from dojo import models as dojo_models


class AvailabilityReportForm(forms.Form):
    available = forms.BooleanField(
        required=False
    )

    session = forms.ModelChoiceField(
        queryset=dojo_models.Session.objects.all()
    )
