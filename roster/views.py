from django.shortcuts import render, redirect
from django.contrib import messages

from dojo import models as dojo_models
from mentors import models as mentor_models
from django.views import generic
from django.utils import timezone

import models
import forms


class ReportView(generic.View):
    def get(self, request, *args, **kwargs):
        # Get the Mentor for this user.
        mentor = mentor_models.Mentor.objects.get(user=request.user)

        relevant_sessions = dojo_models.Session.objects.filter(
            start__gt=timezone.now())

        if not self.request.user.is_superuser:
            relevant_sessions = relevant_sessions.filter(
                term__dojo__role__approved_roles=mentor,
            )

        return render(request, 'roster/availabilities/report.html', {
            'unreported_sessions': relevant_sessions.exclude(
                availability__mentor=mentor
            ),
            'reported_sessions': models.Availability.objects.filter(
                mentor=mentor,
                session__start__gt=timezone.now()
            )
        })

    def post(self, request, *args, **kwargs):
        form = forms.AvailabilityReportForm(request.POST)
        if form.is_valid():
            mentor = mentor_models.Mentor.objects.get(user=request.user)
            # Look for an existing availability for this mentor.
            try:
                avail = models.Availability.objects.get(
                    mentor=mentor,
                    session=form.cleaned_data['session']
                )
            except models.Availability.DoesNotExist:
                # couldn't find an avail,
                avail = models.Availability(
                    mentor=mentor,
                    session=form.cleaned_data['session']
                )

            avail.available = (form.cleaned_data['available'] == True)
            avail.full_clean()
            avail.save()
            messages.success(request, "Your availability for '{a.session}' "
                                      "has been updated!".format(a=avail))
            return redirect('roster:report')
        else:
            print form.errors
