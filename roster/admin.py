from django.contrib import admin

import models


@admin.register(models.Availability)
class AvailabilityAdmin(admin.ModelAdmin):
    list_display = ['mentor', 'session']
