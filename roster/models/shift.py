from django.db import models


class Shift(models.Model):
    mentor = models.ForeignKey(
        'mentors.Mentor',
        help_text='The mentor who is taking this shift.'
    )

    session = models.ForeignKey(
        'dojo.Session',
        help_text='The session where this shift will take place.'
    )

    role = models.ForeignKey(
        'mentors.Role',
        help_text='The role the mentor will be taking.'
    )

    start = models.DateTimeField(
        help_text='The starting time of the shift.'
    )

    finish = models.DateTimeField(
        help_text='The finishing time of the shift.'
    )

    def clean(self):
        if not self.start:
            self.start = self.session.start

        if not self.finish:
            self.finish = self.session.finish
