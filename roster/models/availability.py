from django.db import models


class Availability(models.Model):
    mentor = models.ForeignKey(
        'mentors.Mentor',
        help_text='The mentor that\'s listing this availability.',
    )

    session = models.ForeignKey(
        'dojo.Session',
        help_text='The session that the mentor is available for.',
    )

    available = models.BooleanField(
        default=True,
        help_text='The mentor\'s availability for this session.',
    )

    class Meta:
        verbose_name_plural = 'Availabilities'
        ordering = ['session__start']
