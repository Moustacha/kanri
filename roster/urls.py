from django.conf.urls import patterns, url
from django.contrib import admin

from roster import views


admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^report/', views.ReportView.as_view(), name='report'),
)
