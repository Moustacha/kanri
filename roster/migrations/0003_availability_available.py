# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('roster', '0002_auto_20150311_1418'),
    ]

    operations = [
        migrations.AddField(
            model_name='availability',
            name='available',
            field=models.BooleanField(default=True, help_text=b"The mentor's availability for this session."),
            preserve_default=True,
        ),
    ]
