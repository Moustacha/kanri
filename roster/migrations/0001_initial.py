# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dojo', '0001_initial'),
        ('mentors', '0003_auto_20150311_1346'),
    ]

    operations = [
        migrations.CreateModel(
            name='Availability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('mentor', models.ForeignKey(help_text=b"The mentor that's listing this availability.", to='mentors.Mentor')),
                ('session', models.ForeignKey(help_text=b'The session that the mentor is available for.', to='dojo.Session')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
