# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attendance', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attendance',
            name='arrival',
            field=models.DateTimeField(help_text=b'When did the ninja arrive?', auto_now_add=True),
            preserve_default=True,
        ),
    ]
