from django.conf.urls import patterns, url
from django.contrib import admin

import views

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(
        r'^(?P<pk>\d+)/$',
        views.RecordAttendanceView.as_view(),
        name='record'
    ),

    url(
        r'^ajax$',
        views.RecordAttendanceAjaxView.as_view(),
        name='record-ajax'
    ),

    url(
        r'^$',
        views.SessionListView.as_view(),
        name='list'
    ),
)
