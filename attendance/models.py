from django.db import models


class Attendance(models.Model):
    arrival = models.DateTimeField(
        help_text="When did the ninja arrive?",
        auto_now_add=True,
    )

    sighted_by = models.ForeignKey(
        'mentors.Mentor',
        help_text='Which mentor sighted this Ninja upon arrival and '
                  'ensured their safety and supervision?'
    )

    ninja = models.ForeignKey(
        'ninjas.Ninja',
        help_text='What ninjas is attending?',
    )

    session = models.ForeignKey(
        'dojo.Session',
        help_text='What session is the Ninja attending?',
    )

    def __str__(self):
        return "{s.ninja} attending {s.session}".format(s=self)
