from django.views import generic
from django import shortcuts
from django import http
from django.utils import timezone

import datetime

import dojo
import mentors

import forms
import models


class SessionListView(generic.TemplateView):

    def get_context_data(self, *args, **kwargs):
        c = super(SessionListView, self).get_context_data(*args, **kwargs)

        mentor = mentors.models.Mentor.objects.get(user=self.request.user)
        # this buffer accounts for sessions running over, taking signups
        # early, etc.
        buff = datetime.timedelta(hours=1)
        now = timezone.now()

        relevant = dojo.models.Session.objects.all()

        if not self.request.user.is_superuser:
            relevant = relevant.filter(
                term__dojo__role__approved_roles=mentor)

        # Sessions happening now.
        c['current'] = relevant.filter(
            start__lt=now + buff,
            finish__gt=now - buff
        )

        # Upcomming sessions
        c['upcomming'] = relevant.filter(
            start__gt=now + buff,
            start__lt=now + datetime.timedelta(days=1),
        )

        # Sessions in the 'distant' future
        c['future'] = relevant.filter(
            start__gt=now + datetime.timedelta(days=1),
        )

        # Old sessions.
        c['old'] = relevant.filter(
            finish__lt=now - buff
        )

        return c

    template_name = 'attendance/list.html'


class RecordAttendanceView(generic.ListView):
    template_name = 'attendance/record.html'

    def get_queryset(self, *args, **kwargs):
        self.session = shortcuts.get_object_or_404(
            dojo.models.Session,
            pk=self.kwargs['pk'])

        attn_ninjas = self.session.term.enrolled_ninjas.all()
        return [{
            'ninja': n,
            'attendance': models.Attendance.objects.filter(
                ninja=n, session=self.session)
            } for n in attn_ninjas]

    def get_context_data(self, **kwargs):
        context = super(RecordAttendanceView, self).get_context_data(**kwargs)
        context['session'] = self.session
        context['superuser_override'] = self.superuser_override
        return context

    def dispatch(self, *args, **kwargs):
        self.superuser_override = False

        try:
            self.mentor = mentors.models.Mentor.objects.get(
                user__id=self.request.user.id
            )

            allowed = dojo.models.Session.objects.filter(
                term__dojo__role__approved_roles=self.mentor,
                id=kwargs['pk']
            )
        except mentors.models.Mentor.DoesNotExist:
            allowed = None

        if not allowed:
            if self.request.user.is_superuser:
                self.superuser_override = True
            else:
                return shortcuts.redirect('attendance:list')

        return super(RecordAttendanceView, self).dispatch(*args, **kwargs)


class RecordAttendanceAjaxView(generic.View):
    def post(self, *args, **kwargs):
        form = forms.AttendanceAjaxForm(self.request.POST)
        if form.is_valid():
            if form.cleaned_data['here']:
                att = models.Attendance.objects.create(
                    ninja=form.cleaned_data['ninja'],
                    session=form.cleaned_data['session'],
                    sighted_by=mentors.models.Mentor.objects.get(
                        user=self.request.user))

                att.full_clean()
                att.save()
            else:
                models.Attendance.objects.filter(
                    ninja=form.cleaned_data['ninja'],
                    session=form.cleaned_data['session']
                ).delete()

            return http.JsonResponse({
                'success': True
                })
        else:
            return http.JsonResponse({
                'error': True
                })
