# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0004_role'),
    ]

    operations = [
        migrations.AddField(
            model_name='mentor',
            name='applied_roles',
            field=models.ManyToManyField(help_text=b'The roles this mentor is willing to take.', related_name='applied_roles', to='mentors.Role', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mentor',
            name='approved_roles',
            field=models.ManyToManyField(help_text=b'The roles this mentor has been approved for.', related_name='approved_roles', to='mentors.Role', blank=True),
            preserve_default=True,
        ),
    ]
