# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0006_auto_20150311_1506'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mentor',
            name='mobile',
            field=models.CharField(help_text=b"The mentor's mobile contact number.", max_length=13),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='mentor',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, help_text=b'The user account to associate with this mentor.', unique=True),
            preserve_default=True,
        ),
    ]
