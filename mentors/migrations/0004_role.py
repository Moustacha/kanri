# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0003_auto_20150311_1346'),
    ]

    operations = [
        migrations.CreateModel(
            name='Role',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'A verbose human-readable name for this role.', unique=True, max_length=15)),
                ('description', models.TextField(unique=True, max_length=b'A description of the role.')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
