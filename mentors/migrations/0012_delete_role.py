# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0011_auto_20150314_0858'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Role',
        ),
    ]
