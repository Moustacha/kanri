# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mentors', '0008_mentor_enrolments'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mentor',
            name='enrolments',
        ),
    ]
