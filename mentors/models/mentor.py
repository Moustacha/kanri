from django.db import models
from django.conf import settings

import re


class Mentor(models.Model):
    MALE = 'M'
    FEMALE = 'F'
    OTHER = 'O'

    GENDER_CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
        (OTHER, 'Other'),
    )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        help_text='The user account to associate with this mentor.',
        unique=True
    )

    gender = models.CharField(
        max_length=1,
        choices=GENDER_CHOICES
    )

    # FIXME: (length) work out why clean() runs after length
    # checks are executed.
    mobile = models.CharField(
        max_length=13,
        help_text="The mentor's mobile contact number.",
    )

    applied_roles = models.ManyToManyField(
        'dojo.Role',
        help_text='The roles this mentor is willing to take.',
        related_name='applied_roles',
        blank=True,
    )

    approved_roles = models.ManyToManyField(
        'dojo.Role',
        help_text='The roles this mentor has been approved for.',
        related_name='approved_roles',
        blank=True,
    )

    def clean(self):
        # Clean up for people that (in code order).
        # - use 61 instead of 0
        # - put spaces/dashes.etc in their mobiles.
        # - Lost their 0 to Google Sheet's 'number' cell type.
        # there are definitely better ways of doing this.
        self.mobile = re.sub(r'^\+*61', '0', self.mobile)
        self.mobile = re.sub(r'[^\d]', '', self.mobile)
        self.mobile = re.sub(r'^([^0])', '0\g<0>', self.mobile)

    def __str__(self):
        return self.user.get_full_name()

    class Meta:
        ordering = ['user__first_name']
